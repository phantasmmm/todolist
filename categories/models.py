from django.db import models
from django.conf import settings

from users.models import User
# Create your models here.


class Category(models.Model):
    title = models.CharField('Название', max_length=255)
    text = models.TextField('Описание')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'category_user', null=True)

    def __str__(self):
        return self.title