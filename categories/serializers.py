from rest_framework import serializers

from categories.models import Category
from users.models import User


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('__all__')

    def create(self, validated_data):
        user = self.context.get('request').user
        category = Category.objects.create(user=user, **validated_data)
        return category

