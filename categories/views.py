from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from categories.models import Category
from categories.permission import IsCategoryOwnerOrReadOnly
from categories.serializers import CategorySerializer
from users.models import User
# Create your views here.

class CategoryView(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all
    lookup_field = 'pk'
    permission_classes = (IsCategoryOwnerOrReadOnly,)

    def list(self, request, *args, **kwargs):
        user = request.user
        queryset = Category.objects.filter(user=user)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
