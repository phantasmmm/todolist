from django.db import models
from django.conf import settings

from categories.models import Category
from users.models import User

# Create your models here.


class Task(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    text = models.TextField('Описание', null=True)
    deadline = models.DateTimeField('Дэдлайн', null=True)
    category = models.ForeignKey('categories.Category', models.CASCADE, related_name='task_category', null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'task_user', null=True)

    def __str__(self):
        return self.title