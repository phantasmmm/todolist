from rest_framework import serializers

from users.models import User
from tasks.models import Task
from categories.models import Category


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('__all__')

    def create(self, validated_data):
        user = self.context.get('request').user
        task = Task.objects.create(user=user, **validated_data)
        return task
