from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from tasks.models import Task
from tasks.serializers import TaskSerializer
from tasks.permission import IsTaskOwnerOrReadOnly
from users.models import User

# Create your views here.
class TaskView(ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.prefetch_related('category')
    lookup_field = 'pk'
    permission_classes = (IsTaskOwnerOrReadOnly, )

    def list(self, request, *args, **kwargs):
        user = request.user
        queryset = Task.objects.filter(user=user)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

