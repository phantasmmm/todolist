from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import User
from .permission import IsUserOwnerOrReadOnly
from users.serializers import UserSerializer
# Create your views here.


class UserView(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_class = (IsUserOwnerOrReadOnly, )
    lookup_field = 'pk'